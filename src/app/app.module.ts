import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { FileExplorerComponent } from './file-explorer/file-explorer.component';
import { TreeItemComponent } from './tree-item/tree-item.component';
import { FileTreeService } from './shared/file-tree.service';
import { ItemDetailsComponent } from './item-details/item-details.component';
import { ModalComponent } from './modal/modal.component';
import { DndModule } from 'ng2-dnd';
import { CreateNewComponent } from './create-new/create-new.component';
import { ConfirmComponent } from './confirm/confirm.component';

@NgModule({
  declarations: [
    AppComponent,
    FileExplorerComponent,
    TreeItemComponent,
    ItemDetailsComponent,
    ModalComponent,
    CreateNewComponent,
    ConfirmComponent
],
  imports: [
    BrowserModule,
    FormsModule,
    DndModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [FileTreeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
