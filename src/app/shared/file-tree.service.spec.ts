/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FileTreeService } from './file-tree.service';
import { MockTreeData } from '../constants/mockTreeData';
import { File, Folder } from '../models';

let service: FileTreeService = <any>{};

describe('Service: FileTree', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FileTreeService]
    });
  });

  beforeEach(inject([FileTreeService], (svc) => {
    service = svc;
  }));

  beforeEach(() => {
    service.tree = Object.assign(new Folder('', [...MockTreeData]));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should have mock data initially', (done) => {
    service.tree$.subscribe(res => {
      expect(res.children).toEqual(MockTreeData);
      done();
    });
  });

  describe('add-remove', () => {
    it('should add folder in a sorted position', () => {
      service.addItem(service.tree, new Folder('cmd'));
      expect(service.tree.children.length).toBe(5);
      expect(service.tree.children[1].name).toBe('cmd');
      expect(service.tree.children[1] instanceof Folder).toBe(true);
    });
    
    it('should add folder to the last position among folders', () => {
      service.addItem(service.tree, new Folder('zzz'));
      expect(service.tree.children.length).toBe(5);
      expect(service.tree.children[2].name).toBe('zzz');
      expect(service.tree.children[2] instanceof Folder).toBe(true);
    });

    it('should add file in a sorted position', () => {
      service.addItem(service.tree, new File('newfile.txt'));
      expect(service.tree.children.length).toBe(5);
      expect(service.tree.children[2].name).toBe('newfile.txt');
      expect(service.tree.children[2] instanceof File).toBe(true);
    });

    it('should add file to the end of array', () => {
      service.addItem(service.tree, new File('zzz'));
      expect(service.tree.children.length).toBe(5);
      expect(service.tree.children[4].name).toBe('zzz');
    });

    it('should add file to the empty folder', () => {
      const emptyFolder = <Folder>service.tree.children[1];
      service.addItem(emptyFolder, new File('newfile.txt'));
      expect(emptyFolder.children.length).toBe(1);
      expect(emptyFolder.children[0].name).toBe('newfile.txt');
    });

    it('should add file to the first position', () => {
      const folder = <Folder>service.tree.children.find(f => f instanceof Folder);
      folder.children = [];
      folder.children.push(new File('bbb.txt'));
      service.addItem(folder, new File('aaa.txt'));
      expect(folder.children.length).toBe(2);
    });

    it('should remove file', () => {
      const file = <File>service.tree.children.find(f => f instanceof File);
      const prevCount = service.tree.children.length;
      service.removeItem(file);
      expect(service.tree.children.length).toBe(prevCount - 1);
    });

    it('should remove last file without issues', () => {
      
      const folder = <Folder>service.tree.children.find(f => f instanceof Folder);
      const file = new File('newfile');
      file.parent = folder;
      folder.children = [];
      folder.children.push(file);
      service.removeItem(file);
      expect(folder.children.length).toBe(0);
    });
  });


  it('should emit file selected', (done) => {

    const file = new File('doesnt matter');
    service.selectedFile$.subscribe(f => {
      expect(f).toBe(file);
      done();
    });

    service.selectItem(file);
  });

  it('should save file changes when fileSave invoked', () => {
    const oldTree = service.tree;
    const file = <File>service.tree.children.find(f => f instanceof File);
    const anotherFile = Object.assign(new File(''), file, { contents: 'new contents' });
    service.saveFile(file, anotherFile);
    expect(service.tree.children.find(f => (f instanceof File) && (f.contents === 'new contents')));
  });
});
