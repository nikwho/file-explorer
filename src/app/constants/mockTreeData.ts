import { Folder, File } from '../models';

export const MockTreeData = [
    new Folder('bin', [new File('git.exe'), new File('command.bat')]),
    new Folder('etc'),
    new File('readme.txt'),
    new File('someotherFile.txt'),
  ];
