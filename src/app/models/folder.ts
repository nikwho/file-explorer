import { File } from './file';
export class Folder {
  highlight: boolean;
    constructor(public name: string, public children: Array<Folder | File> = []) {
        children.forEach(c => c.parent = this);
    }
    public parent: Folder;
}
