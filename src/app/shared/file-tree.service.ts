import { Injectable } from '@angular/core';
import { Folder, File } from '../models';
import { MockTreeData } from '../constants/mockTreeData';
import { Subject, BehaviorSubject } from 'rxjs/rx';

@Injectable()
export class FileTreeService {

    tree: Folder;
    tree$: BehaviorSubject<Folder>;
    selectedFile$ = new Subject<File | Folder>();

    constructor() {
        this.tree = new Folder('/', MockTreeData);
        this.tree$ = new BehaviorSubject(this.tree);
    }

    addItem(target: Folder, item: File | Folder) {
        item.parent = target;
        if (target.children.length === 0) {
            target.children.push(item);
            return;
        }    

        //find the index of the item which should be before item we're trying to add
        const indexToInsert = target.children.reduce((acc, current, ix) => {
            if (acc > -1) {
                return acc;
            }
            // only insert after folders
            if (this.compareFn(current, item) > 0) {
                acc = ix;
            }
            return acc;
        }, -1);

        target.children.splice(indexToInsert === -1 ? target.children.length : indexToInsert, 0, item);
        
    }

    // compare function for folders and files. folders should come before files 
    // and folders and files should be sorted alphabetically. 
    // here we assume that simple comparison of strings is a reliable method for sorting
    private compareFn(a: File | Folder, b: File | Folder) {
        if (a.constructor !== b.constructor) {
            return a instanceof Folder ? -1 : 1;
        }
        return a.name > b.name ? 1 : -1;
    }

    removeItem(item: File | Folder) {
        item.parent.children = item.parent.children.filter(child => child !== item);
    }

    selectItem(file: File | Folder) {
        this.selectedFile$.next(file);
    }

    saveFile(oldFile: File, newFile: File) {
        oldFile = Object.assign(oldFile, newFile, { date: new Date() });
        this.selectItem(null);
    }
}

