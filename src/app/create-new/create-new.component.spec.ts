/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CreateNewComponent } from './create-new.component';
import { FormsModule } from '@angular/forms';

describe('CreateNewComponent', () => {
  let component: CreateNewComponent;
  let fixture: ComponentFixture<CreateNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ CreateNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit name on save', () => {
    component.save.emit = jasmine.createSpy('emit').and.stub();
    component.newName = 'test';
    component.onSave();
    expect(component.save.emit).toHaveBeenCalledWith('test');
  });

  it('should emit on cancel', () => {
    component.cancel.emit = jasmine.createSpy('emit').and.stub();
    component.onCancel();
    expect(component.cancel.emit).toHaveBeenCalled();
  });
});
