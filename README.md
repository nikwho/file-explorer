# NgFileExplorer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Requirements

[NodeJS](https://nodejs.org/)

[npmjs](https://www.npmjs.com/)

## Installing

Before running the app, execute from the root folder `npm install` (npm and node should be installed, see Requirements)

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Checking test coverage

Run `npm run coverage` to execute unit tests and generate Coverage report. It can be then found on coverage\index.html
