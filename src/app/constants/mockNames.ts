export const MockNames =
    [
        { name: { title: 'miss', first: 'maïwenn', last: 'lambert' } },
        { name: { title: 'miss', first: 'مارال', last: 'جعفری' } },
        { name: { title: 'mr', first: 'frankie', last: 'bowman' } },
        { name: { title: 'ms', first: 'lotta', last: 'justi' } },
        { name: { title: 'miss', first: 'sedef', last: 'nalbantoğlu' } },
        { name: { title: 'mr', first: 'jesse', last: 'nurmi' } },
        { name: { title: 'mrs', first: 'nora', last: 'tucker' } },
        { name: { title: 'miss', first: 'aino', last: 'luoma' } },
        { name: { title: 'miss', first: 'carmen', last: 'campos' } },
        { name: { title: 'miss', first: 'aurora', last: 'brown' } }
    ];
