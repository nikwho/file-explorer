import { Component, Input, EventEmitter, Output, SimpleChanges, OnChanges } from '@angular/core';
import { Folder, File } from '../models';

@Component({
  selector: 'ca-confirm',
  templateUrl: './confirm.component.html'
})
export class ConfirmComponent implements OnChanges {

  @Input()
  item: File | Folder;

  isFolder: boolean;

  @Output()
  delete = new EventEmitter();

  @Output()
  cancel = new EventEmitter();

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if ('item' in changes) {
      this.isFolder = changes.item.currentValue instanceof Folder;
    }    
  }

  ngOnInit() {
    this.isFolder = this.item instanceof Folder;
  }

  onDelete() {
    this.delete.emit();
  }

  onCancel() {
    this.cancel.emit();
  }

}
