/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ItemDetailsComponent } from './item-details.component';
import { File } from '../models';
import { FormsModule } from '@angular/forms';
import { Folder } from 'app/models/folder';

describe('ItemDetailsComponent', () => {
  let component: ItemDetailsComponent;
  let fixture: ComponentFixture<ItemDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ItemDetailsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemDetailsComponent);
    component = fixture.componentInstance;
    component.item = new File('newFile.txt');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should use copy of an item passed to it', () => {
    component.item =  new File('test.txt');
    component.ngOnChanges(<any>{item: {currentValue: component.item}});
    expect(component.editModel).toEqual(<File>component.item);
    expect(component.editModel).not.toBe(<File>component.item);
  });

  it('should emit saved model when clicking save', () => {
    component.save.emit = <any>jasmine.createSpy('emit').and.stub();
    component.onSave();
    expect(component.save.emit).toHaveBeenCalledWith(component.editModel);
  });

  it('should correctly set isFile', () => {
    component.ngOnChanges(<any>{item:{ currentValue: new File('')}});
    expect(component.isFile).toBe(true);
    component.ngOnChanges(<any>{item:{ currentValue: new Folder('')}});
    fixture.detectChanges();
    expect(component.isFile).toBe(false);
  });
});
