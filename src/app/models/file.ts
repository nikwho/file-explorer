import { MockNames } from '../constants/mockNames';
import { Folder } from './folder';

export class File {
    constructor(public name: string) {
        this.date = new Date();
        const rand = Math.floor((Math.random() * 10));
        this.author = `${MockNames[rand].name.first} ${MockNames[rand].name.last}`;
        this.contents = '';
    }

    public date: Date;
    public author: string;
    public contents: string;
    public parent: Folder;
}
