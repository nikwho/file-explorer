import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'ca-create-new',
  templateUrl: './create-new.component.html'
})
export class CreateNewComponent {

  constructor() { }

  @Output()
  save = new EventEmitter<string>();

  @Output()
  cancel = new EventEmitter();

  newName: string;

  onSave() {
    this.save.emit(this.newName);
    this.newName = '';
  }

  onCancel() {
    this.cancel.emit();
    this.newName = '';
  }
}
