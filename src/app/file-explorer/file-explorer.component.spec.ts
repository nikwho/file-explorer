/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';

import { FileExplorerComponent } from './file-explorer.component';
import { Folder, File } from '../models';
import { FileTreeService } from '../shared/file-tree.service';
import { MockTreeData } from '../constants/mockTreeData';
import { Subject } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ModalComponent } from '../modal/modal.component';

describe('FileExplorerComponent', () => {
  let component: FileExplorerComponent;
  let fixture: ComponentFixture<FileExplorerComponent>;

  const fileTreeServiceMock = {
    tree$: new BehaviorSubject<Folder>(Object.assign(new Folder('/'), { children: MockTreeData })),
    selectedFile$: new Subject<File | Folder>(),
    saveFile: jasmine.createSpy('saveFile').and.stub(),
    addItem: jasmine.createSpy('addItem').and.stub(),
    removeItem: jasmine.createSpy('removeItem').and.stub(),
    selectItem: jasmine.createSpy('selecteItem').and.stub()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileExplorerComponent],
      providers: [
        { provide: FileTreeService, useValue: fileTreeServiceMock }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileExplorerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.createModal.hide = jasmine.createSpy('createhide').and.stub();
    component.createModal.show = jasmine.createSpy('createshow').and.stub();
    component.deleteModal.hide = jasmine.createSpy('deletehide').and.stub();
    component.deleteModal.show = jasmine.createSpy('deleteshow').and.stub();
    fileTreeServiceMock.removeItem.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  describe('seed folder structure', () => {
    it('should be initialized', () => {
      expect(component.tree.children.length).toBeGreaterThan(0);
    });

    it('should have 2 folders and 2 files', () => {
      expect(component.tree.children[0] instanceof Folder).toBe(true);
      expect(component.tree.children[1] instanceof Folder).toBe(true);
      expect(component.tree.children[2] instanceof File).toBe(true);
      expect(component.tree.children[3] instanceof File).toBe(true);

      expect(component.tree.children[0].name < component.tree.children[1].name).toBe(true);
      expect(component.tree.children[2].name < component.tree.children[3].name).toBe(true);
    });
  });

  describe('Details', () => {
    it('should show details component when item emits selected file', () => {
      expect(component.selectedItem).toBeFalsy();
      const fileToSelect = new File('doesnt matter');
      fileTreeServiceMock.selectedFile$.next(fileToSelect);
      fixture.detectChanges();
      expect(component.selectedItem).toBe(fileToSelect);
    });

    it('should show details component when item emits selected folder', () => {
      expect(component.selectedItem).toBeFalsy();
      const fileToSelect = new Folder('doesnt matter');
      fileTreeServiceMock.selectedFile$.next(fileToSelect);
      fixture.detectChanges();
      expect(component.selectedItem).toBe(fileToSelect);
    });
  });

  describe('Create modal dialog', () => {
    it('should open modal on new file', () => {
      component.onNewFile();
      expect(component.createModal.show).toHaveBeenCalled();
    });

    it('should open modal on new folder', () => {
      component.onNewFolder();
      expect(component.createModal.show).toHaveBeenCalled();
    });

    it('should close modal on cancel', () => {
      component.onCancel(component.createModal);
      expect(component.createModal.hide).toHaveBeenCalled();
    });

    it('should close modal on file create', () => {
      component.onCreate('someName');
      expect(component.createModal.hide).toHaveBeenCalled();
    });

    
  });

  describe('Delete modal dialog', () => {
    it('should open modal on confirm delete', () => {
      component.onConfirmDelete(new File('test.txt'));
      expect(component.deleteModal.show).toHaveBeenCalled();
    });

    it('should close modal on delete', () => {
      component.onDelete();
      expect(component.deleteModal.hide).toHaveBeenCalled();
    });

    it('should close modal on cancel', () => {
      component.onCancel(component.deleteModal);
      expect(component.deleteModal.hide).toHaveBeenCalled();
    });
  });
  describe('create file/folder', () => {
    beforeEach(() => {
      fileTreeServiceMock.addItem.calls.reset();
      fileTreeServiceMock.saveFile.calls.reset();
    });

    it('should call service when nothing is selected', () => {
      component.selectedItem = null;
      component.onCreate('someName');
      expect(fileTreeServiceMock.addItem).toHaveBeenCalled();
      const args = fileTreeServiceMock.addItem.calls.mostRecent().args;
      expect(args[0].name).toBe('/');
      expect(args[1].name).toBe('someName');
    });

    it('should call service to create if folder is selected', () => {
      component.selectedItem = new Folder('');
      component.onCreate('someName');
      expect(fileTreeServiceMock.addItem).toHaveBeenCalled();
    });

    it('should not call service to create if file is selected', () => {
      component.selectedItem = new File('');
      component.onCreate('someName');
      expect(fileTreeServiceMock.addItem).not.toHaveBeenCalled();
    });

    it('should set isFolderCreate to true when creating folder', () => {
      component.onNewFolder();
      expect(component.isFolderCreate).toBe(true);
    });

    it('should set isFolderCreate to false when creating file', () => {
      component.onNewFile();
      expect(component.isFolderCreate).toBe(false);
    });

    it('should pass folder with a new name upon creation when isFolderCreate', () => {
      component.selectedItem = new Folder('');
      component.isFolderCreate = true;
      component.onCreate('someName');
      const arg = fileTreeServiceMock.addItem.calls.mostRecent().args[1];
      expect(arg instanceof Folder).toBe(true);
      expect(arg.name).toBe('someName');
    });

    it('should pass file with a new name upon creation when isFolderCreate=false', () => {
      component.selectedItem = new Folder('');
      component.isFolderCreate = false;
      component.onCreate('someName');
      const arg = fileTreeServiceMock.addItem.calls.mostRecent().args[1];
      expect(arg instanceof File).toBe(true);
      expect(arg.name).toBe('someName');
    });
  });

  it('should call service onFileSave', () => {
    const file = <File>MockTreeData.find(s => s instanceof File);
    component.selectedItem = file;
    const newFile = Object.assign(new File(''), file, { contents: 'changed contents' });
    component.onFileSave(newFile);
    expect(fileTreeServiceMock.saveFile).toHaveBeenCalledWith(file, newFile);
  });

  describe('item deletion', () => {
    it('should call service onDelete when item is selected', () => {
      const item = new File('');
      component.selectedItem = item;
      component.onDelete();
      expect(fileTreeServiceMock.removeItem).toHaveBeenCalledWith(item);
      expect(fileTreeServiceMock.selectItem).toHaveBeenCalledWith(null);
    });

    it('should not call service onDelete when no item is selected', () => {
      component.selectedItem = null;
      component.onDelete();
      expect(fileTreeServiceMock.removeItem).not.toHaveBeenCalled();
    });
  });
});
