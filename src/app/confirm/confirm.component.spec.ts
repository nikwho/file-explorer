/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ConfirmComponent } from './confirm.component';
import { Folder, File } from '../models';

describe('ConfirmComponent', () => {
  let component: ConfirmComponent;
  let fixture: ComponentFixture<ConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmComponent);
    component = fixture.componentInstance;
    component.item = new File('asdfasd');
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit delete', () => {
    component.delete.emit = jasmine.createSpy('emit').and.stub();
    component.onDelete();
    expect(component.delete.emit).toHaveBeenCalled();
  });

  it('should emit cancel', () => {
    component.cancel.emit = jasmine.createSpy('emit').and.stub();
    component.onCancel();
    expect(component.cancel.emit).toHaveBeenCalled();
  });

  it('should correctly set isFolder on item change', ()=> {
    component.ngOnChanges(<any>{item: { currentValue: new File('')}});
    expect(component.isFolder).toBe(false);
    component.ngOnChanges(<any>{item: { currentValue: new Folder('')}});
    expect(component.isFolder).toBe(true);
  });
});
