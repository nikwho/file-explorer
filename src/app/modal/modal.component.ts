import { Component, OnInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'ca-modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent implements OnInit {
  @ViewChild('template') 
  template;
  
  @Input()
  title;

  ngOnInit(): void {
    
  }
  modalRef: BsModalRef;
  constructor(private modalService: BsModalService) {}
 
  show() {
    this.modalRef = this.modalService.show(this.template);
  }
  hide() {
    this.modalRef.hide();
  }
}
