import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Folder, File } from '../models';
import { FileTreeService } from '../shared/file-tree.service';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'ca-file-explorer',
  templateUrl: './file-explorer.component.html'
})
export class FileExplorerComponent {

  @ViewChild('createModal')
  createModal: ModalComponent;

  @ViewChild('deleteModal')
  deleteModal: ModalComponent;


  tree: Folder;
  selectedItem: File | Folder;
  isFolderCreate: boolean;
  isFolderSelected: boolean;

  constructor(private fileTreeService: FileTreeService) {
    this.isFolderSelected = true;
    fileTreeService.tree$.subscribe(v => this.tree = v);
    fileTreeService.selectedFile$.subscribe(f => {      
      this.selectedItem = f;      
      this.isFolderSelected = this.selectedItem === null || this.selectedItem instanceof Folder;
    });
  }

  onFileSave(modifiedFile) {
    if (this.selectedItem instanceof File) {
      this.fileTreeService.saveFile(this.selectedItem, modifiedFile);
      this.selectedItem = null;
    }
  }

  onNewFile() {
    this.isFolderCreate = false;
    this.createModal.show();
  }

  onNewFolder() {
    this.isFolderCreate = true;
    this.createModal.show();
  }

  onCancel(modal: ModalComponent) {
    modal.hide();
  }

  onCreate(newName: string) {
    const itemToCreate = this.isFolderCreate ?
                            new Folder(newName) :
                            new File(newName);

    if (!this.selectedItem) {
      this.fileTreeService.addItem(this.tree, itemToCreate);
    }
    else if (this.selectedItem instanceof Folder) {
      this.fileTreeService.addItem(this.selectedItem, itemToCreate);
    }
    this.createModal.hide();
  }

  onConfirmDelete(item: File | Folder) {
    this.deleteModal.show();
  }

  onDelete() {
    if (this.selectedItem) {
      this.fileTreeService.removeItem(this.selectedItem);
      this.fileTreeService.selectItem(null);
    }
    this.deleteModal.hide();
  }
}
