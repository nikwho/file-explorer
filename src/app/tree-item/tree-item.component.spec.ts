/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';

import { TreeItemComponent } from './tree-item.component';
import { File, Folder } from '../models';
import { FileTreeService } from '../shared/file-tree.service';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs/rx';
import { DndModule } from 'ng2-dnd';




describe('TreeItemComponent', () => {
  let component: TreeItemComponent;
  let fixture: ComponentFixture<TreeItemComponent>;

  const fileTreeServiceMock = {
    selectItem: jasmine.createSpy('selectItem').and.stub(),
    selectedFile$: new Subject<File>(),
    removeItem: jasmine.createSpy('removeItem').and.stub(),
    addItem: jasmine.createSpy('addItem').and.stub()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [TreeItemComponent],
      providers: [
        { provide: FileTreeService, useValue: fileTreeServiceMock }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeItemComponent);
    component = fixture.componentInstance;
    component.item = new Folder('test');
    fixture.detectChanges();
    fileTreeServiceMock.selectItem.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an item in it', () => {
    expect(component.item).toBeDefined();
  });

  it('should emit selected on clicked file', () => {
    component.item = new File('somefile.txt');
    fixture.nativeElement.querySelector('span').dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(fileTreeServiceMock.selectItem).toHaveBeenCalled();
  });

  it('should emit selected on clicked folder', () => {
    fixture.nativeElement.querySelector('span').dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(fileTreeServiceMock.selectItem).toHaveBeenCalled();
  });

  it('should correctly set isFolder', () => {
    component.item = new Folder('');
    component.ngOnInit();
    expect(component.isFolder).toBe(true);
    
    component.item = new File('');
    component.ngOnInit();
    expect(component.isFolder).toBe(false);
  });
  describe('highlight', () => {
    it('should highlight parent when drag over file', () => {
      component.item = new File('');
      component.item.parent = new Folder('');
      component.onDragEnter();
      expect(component.item.parent.highlight).toBe(true);
    });
  
    it('should highlight item if it is folder on drag over', () => {
      component.item = new Folder('');
      component.onDragEnter();
      expect(component.item.highlight).toBe(true);
    });

    it('should un-highlight item if it is folder on drag leave', () => {
      component.item = new Folder('');
      component.item.highlight = true;
      component.onDragLeave();
      expect(component.item.highlight).toBe(false);
    });

    it('should un-highlight item\'s parent if it is folder on drag leave', () => {
      component.item = new File('');
      component.item.parent = new Folder('');
      component.item.parent.highlight = true;
      component.onDragLeave();
      expect(component.item.parent.highlight).toBe(false);
    });
  });

  describe('drop', () => {
    it('should call service when dropped on file', () => {
      const droppedFile = new File('drop');
      droppedFile.parent = new Folder('dropParent');
      component.item = new File('');
      component.item.parent = new Folder('');
      component.drop({dragData: {item : droppedFile }});
      expect(fileTreeServiceMock.removeItem).toHaveBeenCalledWith(droppedFile);
      expect(fileTreeServiceMock.addItem).toHaveBeenCalledWith(component.item.parent, droppedFile);
    });

    it('should call service when dropped on folder', () => {
      const droppedFile = new File('drop');
      droppedFile.parent = new Folder('dropParent');
      component.item = new Folder('');      
      component.drop({dragData: {item : droppedFile }});
      expect(fileTreeServiceMock.removeItem).toHaveBeenCalledWith(droppedFile);
      expect(fileTreeServiceMock.addItem).toHaveBeenCalledWith(component.item, droppedFile);
    });

    it('should reset highlight of target folder', () => {
      const droppedFile = new File('drop');
      droppedFile.parent = new Folder('dropParent');      
      component.item = new Folder('');      
      droppedFile.parent.highlight = true;
      component.drop({dragData: {item : droppedFile }});      
      expect(droppedFile.parent.highlight).toBe(false);
    });

  });

});
