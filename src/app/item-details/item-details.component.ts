import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { File, Folder } from '../models';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ca-item-details',
  templateUrl: './item-details.component.html'
})
export class ItemDetailsComponent implements OnChanges {

  @Input()
  item: File | Folder;

  @Output()
  save = new EventEmitter();

  @Output()
  cancel = new EventEmitter();

  editModel = new File('');

  isFile: boolean;

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if ('item' in changes) {
      if (changes.item.currentValue instanceof File) {
        this.editModel = Object.assign(new File(''), changes.item.currentValue);
      }
      this.isFile = changes.item.currentValue instanceof File;
    }
  }

  onSave() {
    this.save.emit(this.editModel);
  }

  onCancel() {     
    this.cancel.emit();
  }
}
