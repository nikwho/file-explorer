import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation, ChangeDetectionStrategy, Host, Injectable } from '@angular/core';
import { File, Folder } from '../models';
import { FileTreeService } from '../shared/file-tree.service';
import { FileExplorerComponent } from '../file-explorer/file-explorer.component';

@Injectable()
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ca-tree-item',
  templateUrl: './tree-item.component.html',
  styleUrls: ['./tree-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TreeItemComponent implements OnInit {

  @Input()
  item: File | Folder;

  isSelected: boolean;

  constructor(private fileTreeService: FileTreeService) {
    this.fileTreeService.selectedFile$.subscribe(f => {
      this.isSelected = f === this.item;
    });
  }

  isFolder: boolean;

  ngOnInit() {
    this.isFolder = this.item instanceof Folder;
  }

  emitSelected() {
    this.fileTreeService.selectItem(this.item);
  }

  drop(event) {
    this.fileTreeService.removeItem(event.dragData.item);
    if (this.item instanceof Folder) {
      this.fileTreeService.addItem(this.item, event.dragData.item);
    } else {
      this.fileTreeService.addItem(this.item.parent, event.dragData.item );
    }
    event.dragData.item.parent.highlight = false;

  }

  onDragEnter() {
    if (this.item instanceof File) {
      this.item.parent.highlight = true;
    } 
    if (this.item instanceof Folder) {
      this.item.highlight = true;
    }
  }

  onDragLeave() {    
    if (this.item instanceof File) {
      this.item.parent.highlight = false;
    } 
    if (this.item instanceof Folder) {
      this.item.highlight = false;
    }
  }
}